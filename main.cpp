#include <iostream>
#include <time.h>
#include <stdlib.h>

//****************************************
//***      CONTROLE 2 : MASTERMIND     ***
//****************************************
using namespace std;

//Structure d'une partie
struct Jeu
{
    int combinaison[4];
    int choix[4];
    int nbEssais;
    int partiesGagnees=0;
    int partiesPerdues=0;
};

//structure pour le renvoi des r�sultats
struct ResultatCombinaison
{
    int nbBienPlace;
    int nbPresent;
};

//fonction d'affichage : test et affichage du s pour les pluriels
void affichageDuS(int nb)
{
    if (nb>1)
    {
        cout << "s";
    }
}

//fonction qui g�n�re la combinaison de 4 chiffres de 0-9
Jeu genereCombinaison (Jeu partie)
{
    for (int i=0; i<4; ++i)
    {
        partie.combinaison[i]=rand()%10;
    }
    return partie;
}

//saisie de la combinaison joueur avec controle de saisie 0-9
Jeu saisieCombinaison(Jeu partie, int i)
{
    //demande de saisie d'un nombre de la combinaison joueur
    cout << "Entrer le chiffre #" << i+1 << " ";
    cin >> partie.choix[i];
    //test du choix du joueur
    while(partie.choix[i]<0 || partie.choix[i]>9)
    {
        cout << "Entrer la chiffre #" << i+1 << " [0-9]";
        cin >> partie.choix[i];
    }
    return partie;
}

//v�rification de la combinaison / renvoi le nombre de chiffres bien plac�s
ResultatCombinaison VerificationCombinaison(Jeu partie, ResultatCombinaison result)
{
    //r�-initialisation des variables pour le r�sultat
    result.nbBienPlace=0;
    result.nbPresent=0;

    for (int i=0;i<4;++i)
    {
        //v�rifier si le chiffre est bien plac�
        if (partie.choix[i]==partie.combinaison[i])
        {
            result.nbBienPlace++;
        }
        else //sinon v�rifier s'il est pr�sent
        {
            for (int j=0;j<4;++j)
            {
                if (partie.choix[i]==partie.combinaison[j])
                {
                    //s'il est pr�sent, on incr�ment nbPr�sent et on sort de la boucle
                    result.nbPresent++;
                    break;
                }
            }
        }
    }
    //si la combinaison n'est pas la bonne, on affiche le resum� du tour
    if (result.nbBienPlace!=4)
    {
        cout << endl << "\t*********************" << endl << "\t***  RESUME TOUR  ***" << endl << "\t*********************" << endl;
        //affichage du nombre de chiffres bien plac�s
        cout << "Bien place";
        affichageDuS(result.nbBienPlace);
        cout << " : " << result.nbBienPlace << endl;
        //affichage du nombre de chiffres pr�sents
        cout << "Present";
        affichageDuS(result.nbPresent);
        cout << " : " << result.nbPresent << endl;
    }
    return result;
}

//saisie pour continuer la partie avec controle de saisie 0-1
int demandeContinuer(void)
{
    int continuer=0;
    cout << endl << "Voulez-vous commencer une nouvelle partie? (0 pour continuer, 1 pour s'arreter)";
    cin >> continuer;
    while (continuer <0 || continuer>1)
    {
        cout << endl << "Voulez-vous commencer une nouvelle partie? (0 pour continuer, 1 pour s'arreter)";
        cin >> continuer;
    }
    return continuer;
}

int main()
{
    srand(time(NULL));
    //d�claration des variables
    ResultatCombinaison result;

    //structure pour la partie
    Jeu partie;
    //variable permettant de demande au joueur s'il veut commencer une nouvelle partie
    int continuer=0;

    //le jeu continue tant que le joueur veut continuer
    while (continuer==0)
    {
        //initialisation du nombre de chances
        partie.nbEssais=10;
        //initilisation du nombre de chiffres bien plac�s
        result.nbBienPlace=0;

        //initialisation � 0 des tableaux
        for (int i=0; i<4; ++i)
        {
            partie.combinaison[i]=0;
            partie.choix[i]=0;
        }

        //g�n�ration de la combinaison
        partie=genereCombinaison(partie);

        //D�but de la partie
        cout << endl << "\t*********************" << endl << "\t***DEBUT DE PARTIE***" << endl << "\t*********************" << endl;
        cout << "L'ordinateur a genere une suite de 4 chiffres de 0 a 9" << endl;

        //la partie s'arrete quand le joueur n'a plus d'essais
        //ou que le joueur n'a pas trouv� la combinaison
        while (partie.nbEssais>0 && result.nbBienPlace!=4)
        {
            cout << "Vous avez encore " << partie.nbEssais << " essai";
            affichageDuS(partie.nbEssais);
            cout << " pour trouver la bonne combinaison" << endl;
            //saisie des choix par le joueur
            for (int i=0; i<4;++i)
            {
                partie=saisieCombinaison(partie,i);
            }

            //V�rification de la combinaison
            result = VerificationCombinaison(partie, result);

            //d�cr�mentation du nombre d'essai
            partie.nbEssais--;
        }

        //Affichage fin de partie
        cout << endl << "\t*********************" << endl << "\t*** RESUME PARTIE ***" << endl << "\t*********************" << endl;

        //test si la partie est gagn�e ou perdue
        if (partie.nbEssais==0)
        {
            //partie perdue
            cout << "Plus d'essai, partie perdue " << endl;
            partie.partiesPerdues++;
        }
        else //partie gagn�e
        {
            cout << "Partie gagnee! " << endl;
            partie.partiesGagnees++;
        }

        //affichage fin de partie
        cout << "La combinaison etait : ";
        for (int i=0; i<4; ++i)
        {
            cout << partie.combinaison[i] << " ";
        }
        cout << endl << endl << "\t*********************" << endl << "\t*** RESUME SCORES ***" << endl << "\t*********************" << endl;
        //parties gagn�es
        cout << "Partie";
        affichageDuS(partie.partiesGagnees);
        cout << " gagnee";
        affichageDuS(partie.partiesGagnees);
        cout << " : " << partie.partiesGagnees << endl;
        //parties perdues
        cout << "Partie";
        affichageDuS(partie.partiesPerdues);
        cout << " perdue";
        affichageDuS(partie.partiesPerdues);
        cout <<" : " << partie.partiesPerdues << endl;

        continuer=demandeContinuer();
    } //fin du continuer

    cout << "\t*********************" << endl << "\t**   GAME   OVER   **" << endl << "\t*********************";

    return 0;
}
