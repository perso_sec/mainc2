#include <iostream>
#include <time.h>
#include <stdlib.h>



using namespace std;

//Structure d'une partie
struct Jeu
{
    int combinaison[4];
    int choix[4];
    int nbEssais;
    int partiesGagnees=0;
    int partiesPerdues=0;
};

//fonction qui g�n�re la combinaison de 4 chiffres de 0-9
Jeu genereCombinaison (Jeu partie)
{
    for (int i=0; i<4; ++i)
    {
        partie.combinaison[i]=rand()%10;
    }
    return partie;
}

//saisie de la combinaison joueur
Jeu saisieCombinaison(Jeu partie, int i)
{
    //demande de saisie d'un nombre de la combinaison joueur
    cout << "Entrer la combinaison #" << i << " ";
    cin >> partie.choix[i];
    //test du choix du joueur
    while(partie.choix[i]<0 || partie.choix[i]>9)
    {
        cout << "Entrer la combinaison #" << i << " [0-9]";
        cin >> partie.choix[i];
    }
    return partie;
}

//v�rification des nombres bien plac�s
int testNbBienPlace(Jeu partie)
{
    int nbPlace=0;
    for (int i=0;i<4;++i)
    {
        if (partie.combinaison[i]==partie.choix[i])
        {
            nbPlace++;
        }
    }
    return nbPlace;
}

//v�rification des nombres bien plac�s
int testNbPresents(Jeu partie)
{
    int nbPresent=0;
    for (int i=0;i<4;++i)
    {
        for (int j=0;j<4;++j)
        {
            //si le nombre est bien plac�, on ne teste pas s'il est pr�sent
            if (i==j && partie.choix[i]==partie.combinaison[j])
            {
                break;
            }
            //sinon si les 2 nombres sont �gaux
            else if (partie.choix[i]==partie.combinaison[j])
            {
                nbPresent++;
                break;
            }
        }
    }
    return nbPresent;
}

int main()
{
    srand(time(NULL));
    //d�claration des variables
    //structure pour la partie
    Jeu partie;
    //variable permettant de savoir si le joueur veut refaire une partie
    int continuer=0;

    //initialisation � 0 des tableaux
    for (int i=0; i<4; ++i)
    {
        partie.combinaison[i]=0;
        partie.choix[i]=0;
    }

    //g�n�ration de la combinaison
    partie=genereCombinaison(partie);

    /*
    ///test d'affichage de la combinaison
    for (int i=0; i<4; ++i)
    {
        cout << partie.combinaison[i] << endl;
    }
    ///fin test de l'affichage de la combinaison
    */

    //le jeu continue tant que le joueur veut continuer
    while (continuer==0)
    {
        //d�claration/initialisation des variables de la partie
        int nbBienPlace=0;
        int nbPresent=0;
        //initialisation du nombre de chances
        partie.nbEssais=10;

        //D�but de la partie
        cout << "L'ordinateur a genere une suite de 4 chiffres de 0 a 9" << endl;
        //la partie s'arrete quand le joueur n'a plus d'essais
        //ou que le joueur n'a pas trouv� la combinaison
        while (partie.nbEssais>0 && nbBienPlace!=4)
        {
            cout << "Vous avez " << partie.nbEssais << " essais pour trouver la bonne combinaison" << endl;
            //saisie utilisation des choix
            for (int i=0; i<4;++i)
            {
                partie=saisieCombinaison(partie,i);
            }

            //test du nombre d'�l�ments bien plac�s
            nbBienPlace=testNbBienPlace(partie);

            ///test du nbBienPlace
            cout << endl << "bien place : " << nbBienPlace << endl;

            //si on n'a pas trouv� la bonne combinaison, on teste les nombres bien plac�s
            if (nbBienPlace!=4)
            {
                nbPresent=testNbPresents(partie);
                ///test du nbPresent
                cout << endl << "Presents : " << nbPresent << endl;
            }

            //Affichage fin de tour




            partie.nbEssais--;
        }
        cout << "Plus d'essais" << endl;
        cout << "La combinaison �tait : ";
        for (int i=0; i<4; ++i)
        {
            cout << partie.combinaison[i] << " ";
        }
        cout << endl << "Voulez-vous rejouer? (0 pour continuer, 1 pour s'arreter)";
        cin >> continuer;
    } //fin du continuer




    return 0;
}
